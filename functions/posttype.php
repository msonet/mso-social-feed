<?php
//Block direct access
if ( ! defined( 'ABSPATH' ) ) exit;

/** CREATE SOCIAL POSTTYPE AND ADD TO PLUGIN MENU */
add_action( 'init', function() {
    register_post_type('mso-social-posts', [
        'labels' => [
            'name' => __('Social Posts'),
            'singular_name' => __('Social Post'),
            'all_items' => __('Social Posts'),
            'new_item' => __('New Social Post'),
            'add_new' => __('Add New'),
            'add_new_item' => __('Add New Social Post'),
            'edit_item' => __('Edit Social Post'),
            'view_item' => __('View Social Post'),
            'search_items' => __('Search Social Posts'),
            'not_found' => __('No Social posts found'),
            'not_found_in_trash' => __('No Social posts found in trash'),
            'parent_item_colon' => __('Parent Social'),
            'menu_name' => __('Social Posts', 'pennthorpe'),
        ],

        
        'public' => false,
        'hierarchical' => false,
        'show_ui' => true,
        'show_in_nav_menus' => true,
        'show_in_menu' => 'msosocial-feed',
        'supports' => array('title','thumbnail'),
        'has_archive' => false,
        'rewrite' => false,
        'query_var' => false,
        'menu_icon' => 'dashicons-admin-site-alt',
        'show_in_rest' => true,
        'rest_base' => 'social_posts',
        'rest_controller_class' => 'WP_REST_Posts_Controller',
        
    ]);
    flush_rewrite_rules();

    register_taxonomy('social-channels', 
        ['mso-social-posts'], 
        [
        'hierarchical' => false,
        'public' => false,
        'show_in_menu' => false,
        'show_in_nav_menus' => false,
        'show_ui' => false,
        'show_admin_column' => true,
        'query_var' => false,
        'rewrite' => false,
        'capabilities' => [
          'manage_terms'  => 'edit_posts',
          'edit_terms'    => 'edit_posts',
          'delete_terms'  => 'edit_posts',
          'assign_terms'  => 'edit_posts',
        ],
        'show_in_rest' => false,
        'rest_base' => 'social-channels',
        'rest_controller_class' => 'WP_REST_Terms_Controller',
        'labels' => [
            'name' => __('Social Channels', 'pennthorpe'),
            'singular_name' => _x('Social Channel', 'taxonomy general name', 'pennthorpe'),
            'search_items' => __('Search Social Channels', 'pennthorpe'),
            'popular_items' => __('Popular Social Channels', 'pennthorpe'),
            'all_items' => __('All Social Channels', 'pennthorpe'),
            'parent_item' => __('Parent Social Channel', 'pennthorpe'),
            'parent_item_colon' => __('Parent Social Channel:', 'pennthorpe'),
            'edit_item' => __('Edit Social Channel', 'pennthorpe'),
            'update_item' => __('Update Social Channel', 'pennthorpe'),
            'add_new_item' => __('New Social Channel', 'pennthorpe'),
            'new_item_name' => __('New Social Channel', 'pennthorpe'),
            'separate_items_with_commas' => __('Separate Social Channels with commas', 'pennthorpe'),
            'add_or_remove_items' => __('Add or remove Social Channels', 'pennthorpe'),
            'choose_from_most_used' => __('Choose from the most used Social Channels', 'pennthorpe'),
            'not_found' => __('No Social Channels found.', 'pennthorpe'),
            'menu_name' => __('Social Channels', 'pennthorpe'),
        ],
      ]);
});



add_filter( 'post_updated_messages', function ($messages ) {
    global $post;
    $permalink = get_permalink($post);
    $messages['social'] = [
      0 => '',
      1 => sprintf(__('Social post updated. <a target="_blank" href="%s">View post</a>'), esc_url($permalink)),
      2 => __('Custom field updated.'),
      3 => __('Custom field deleted.'),
      4 => __('Social post updated.'),
      5 => isset($_GET['revision']) ? sprintf(__('Social restored to revision from %s'), wp_post_revision_title((int) $_GET['revision'], false)) : false,
      6 => sprintf(__('Social post published. <a href="%s">View post</a>'), esc_url($permalink)),
      7 => __('Social post saved.'),
      8 => sprintf(__('Post submitted. <a target="_blank" href="%s">Preview post</a>'), esc_url(add_query_arg('preview', 'true', $permalink))),
      9 => sprintf(__('Post scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview post</a>'), date_i18n(__('M j, Y @ G:i'), strtotime($post->post_date)), esc_url($permalink)),
      10 => sprintf(__('Post draft updated. <a target="_blank" href="%s">Preview post</a>'), esc_url(add_query_arg('preview', 'true', $permalink))),
    ];
    return $messages;
});

/** CREATE SOCIAL POSTTYPE AND ADD TO PLUGIN MENU */




/** CREATE TAXONOMY TERMS */
add_action( 'admin_init', function() {

    $channels = array(
        'Facebook',
        'Twitter',
        'Instagram'
    );
    foreach($channels as $channel):
        if(!term_exists($channel, 'social-channels') ):
            wp_insert_term($channel,'social-channels');
        endif;
    endforeach;
});
/** CREATE TAXONOMY TERMS */


/** CREATE CUSTOM FIELDS */

add_action('load-post.php', 'mso_social_meta_boxes_setup');
add_action('load-post-new.php', 'mso_social_meta_boxes_setup');

function mso_social_meta_boxes_setup() {
    add_action('add_meta_boxes', 'mso_social_add_post_meta_boxes');
    add_action('save_post', 'mso_social_save_post_meta_boxes', 10, 2);
}

function mso_social_add_post_meta_boxes() {
    //We don't need a slug, we don't want a slug.
    remove_meta_box('slugdiv', 'mso-social-posts', 'normal');

    add_meta_box(
        'social-id',
        esc_html__( 'Social ID', '' ),
        'social_id_meta_box',
        array('mso-social-posts')
    );
    add_meta_box(
        'social-message',
        esc_html__( 'Social Message', '' ),
        'social_message_meta_box',
        array('mso-social-posts')
    );
    add_meta_box(
        'social-link',
        esc_html__( 'Social Link', '' ),
        'social_link_meta_box',
        array('mso-social-posts')
    );
}

function social_id_meta_box( $object, $box ) { ?>
    <?php wp_nonce_field( basename( __FILE__ ), 'social_id_nonce' ); ?>
    <p>
    <input class="widefat" type="text" name="social-id" id="social-id" value="<?php echo esc_attr( get_post_meta( $object->ID, 'social_id', true ) ); ?>" size="30" />
    </p>
<?php }


function social_message_meta_box( $object, $box ) { ?>
    <?php wp_nonce_field( basename( __FILE__ ), 'social_message_nonce' ); ?>
    <p>
    <?php $settings = [
        'textarea_name' => 'social-message',
        'teeny' => true,
        'media_buttons' => false
    ];
    wp_editor( get_post_meta( $object->ID, 'social_message', true ),'social_message', $settings); ?>
    </p>
<?php } 

function social_link_meta_box( $object, $box ) { ?>
    <?php wp_nonce_field( basename( __FILE__ ), 'social_link_nonce' ); ?>
    <p style="display: flex;align-items: center;">
    <input style="flex: 0 1 auto;" class="widefat" type="text" name="social-link" id="social-link" value="<?php echo esc_attr( get_post_meta( $object->ID, 'social_link', true ) ); ?>" size="30" />
    <a style="flex: 1 0 auto; padding-left: 20px;" href="<?php echo esc_attr( get_post_meta( $object->ID, 'social_link', true ) ); ?>" target="_blank">Check link</a>
    </p>
<?php } 



 // Save Meta Box
 function mso_social_save_post_meta_boxes( $post_id, $post ) {
    if (!isset( $_POST['social_id_nonce'] ) || !wp_verify_nonce( $_POST['social_id_nonce'], basename( __FILE__ ) )) {
        return $post_id;
    }
    $post_type = get_post_type_object( $post->post_type );
    if (!current_user_can( $post_type->cap->edit_post, $post_id )) {
        return $post_id;
    }
    $arrayValsToSave = array(
        'social_id',
        'social_message',
        'social_link'
    );
    foreach ($arrayValsToSave as &$dbfieldname) {
        $metafieldName = str_replace("_","-",$dbfieldname);
        $new_meta_value = ( isset( $_POST[$metafieldName] ) ?  $_POST[$metafieldName] : '' );
        $meta_key = $dbfieldname;
        $meta_value = get_post_meta( $post_id, $meta_key, true );
        update_post_meta( $post_id, $meta_key, $new_meta_value );
    }
}
/** CREATE CUSTOM FIELDS */