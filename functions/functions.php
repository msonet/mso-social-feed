<?php
//Block direct access
if ( ! defined( 'ABSPATH' ) ) exit;

//CHECK IF A SOCIAL POST HAS ALREADY BEEN IMPORTED (CHECK BY SOCIAL POST ID)
function check_imported_social_posts($eventImportId) {
    $args = array(
        'posts_per_page'   => 1,
        'post_type'     => 'mso-social-posts',
        'meta_query'    => array(
            array(
                'key'       => 'social_id',
                'value'     => $eventImportId
            )
        )
    );
    $query = new WP_Query($args);
    if ( $query->have_posts() ) {
        return true;
    }
    return false;
}



//DOWNLOAD IMAGE FROM SOCIAL MEDIA TO WORDPRESS
function get_social_image($filename,$url,$parent_post_id,$social_url){
    if(function_exists('post_exists')):
    //Check if image exists and if it does just link it and skip
    $image_exists = post_exists($filename,'','','attachment');
    endif;

    if(function_exists('post_exists') && $image_exists !== 0 ):
        //post_exists return $post->ID if found
        $attach_id = $image_exists;
    else:

        /** DOWNLOAD IMAGE TO MEDIA LIBRARY FROM SOCIAL */
        //Get the image data
        $image = wp_remote_get($url);
        $type = $image['headers']->offsetGet('content-type');
        preg_match('/(jpeg|png|gif)/',$type,$ext);
        
        //Get the upload directory and set the filename
        $uploaddir = wp_upload_dir();
        $uploadfile = $uploaddir['path'] . '/' . $filename.'.'.$ext[0];

        //Save image to upload directory
        $savefile = fopen($uploadfile, 'w');
        fwrite($savefile, $image['body']);
        fclose($savefile);
        /** DOWNLOAD IMAGE TO MEDIA LIBRARY FROM SOCIAL */


        /** ADD UPLOADED IMAGE TO MEDIA LIBRARY */
        $attachment = array(
            'post_mime_type' => $type,
            'post_title' => $filename,
            'post_content' => 'Imported from '.$social_url,
            'post_status' => 'inherit'
        );
        $attach_id = wp_insert_attachment( $attachment, $uploadfile );

        //Create the metadata
        require_once( ABSPATH . 'wp-admin/includes/image.php' );
        $imagenew = get_post( $attach_id );
        $fullsizepath = get_attached_file( $imagenew->ID );
        $attach_data = wp_generate_attachment_metadata( $attach_id, $fullsizepath );
        $attach_data['image_meta']['caption'] = $social_url;
        
        wp_update_attachment_metadata( $attach_id, $attach_data );

    endif;

    //If the post's ID is set then assign image as Featured Image
    if(!empty($parent_post_id)):
        set_post_thumbnail( $parent_post_id, $attach_id );
    endif;
}
//DOWNLOAD IMAGE FROM SOCIAL MEDIA TO WORDPRESS


//IMPORT POSTS TO ARRAY
function download_posts($id,$message,$created_time,$image,$permalink,$channel){
    if(!empty($message)):
        $title = substr( $message,0,40).'...';
    else:
        $title = $id;
    endif;

    $title = preg_replace('/[^\\w\d\s!@£#\$%&\(\):;\,\.\?\-\+]/', "",$title);
    $message = preg_replace('/[^\\w\d\s!@£#\$%&\(\):;\,\.\?\-\+]/', "",$message);


    //Check if Facebook post has already been imported
    $output['message'] = '<tr><td>Checking '.ucfirst($channel).' post <a href="'.$permalink.'" target="_blank">'.$title.'...</a></td>';
    $social_post = check_imported_social_posts($id);

    //If not found, append it to the importing array
    if($social_post === false):
        $output['import'] = [
            'post_title'      => $title,
            'post_date'       => $created_time,
            'social_id'       => $id,
            'social_image'    => $image,
            'social_message'  => $message,
            'social_link'     => $permalink,
            'social_channel'  => [$channel]
        ];
        
        $output['message'] .= '<td><span style="color:red">* NEW *</span></td></tr>';
    else:
        $output['message'] .= '<td>already imported</td></tr>';
    endif;

    return $output;
}
//IMPORT POSTS TO ARRAY


//PREDEFINED WP_QUERY FOR SOCIAL CHANNELS (call in the template files)
/*
NOTE:
get_social_posts(
    $term = 'facebook' or 'twitter' or 'instagram' (required)
    $posts_per_page = [int] (1 by default - optional.)
    $date_format = [string] ('jS F Y' by default - optional.)
    $image_size = [string] ('full' by default - optional. Set to desired registered image size)
    $return_query = [boolean] (false by default - optional. True returns the query only, false runs the query and returns the values in an array)
        [
            'date'      => get_the_date($date_format),
            'image'     => get_the_post_thumbnail_url(get_the_ID(),$image_size),
            'message'   => get_post_meta( get_the_ID(), 'social_message', true ),
            'link'      => get_post_meta( get_the_ID(), 'social_link', true ),
        ]
)
*/

function get_social_posts($term='',$ppp=1,$d_format='jS F Y',$i_size='full',$return_query=false){
    if(empty($term)): return false; endif;

    $query = new WP_Query( array (
        'post_type' => 'mso-social-posts',
        'posts_per_page' => $ppp,
        'tax_query' => array(
            'relation' => 'AND',
            array(
                'taxonomy' => 'social-channels',
                'field'    => 'name',
                'terms'    => $term
            )
        )
    ));

    //Just return the query
    if($return_query===true):
        return $query;
    endif;

    //Or return the values of each necessary field in an array
    if($query->have_posts()): while($query->have_posts()): $query->the_post(); 
    $fields[] = [
        /* Title and Social ID are primarily used for identification in the back end
        so are not included here */
        'date'      => get_the_date($d_formtat),
        'image'     => get_the_post_thumbnail_url(get_the_ID(),$i_size),
        'message'   => get_post_meta( get_the_ID(), 'social_message', true ),
        'link'      => get_post_meta( get_the_ID(), 'social_link', true ),
    ];
    endwhile; endif; wp_reset_postdata();
    return $fields;
}
//PREDEFINED WP_QUERY FOR SOCIAL CHANNELS (call in the template files)





//IMPORT POSTS
function mso_import_posts($display=true){
    
    if($display===true): 
    echo '<div class="social_settings import">';
    endif;
        
        //Throttle automatic import occurences to anytime over an hour since last import
        $last_imported = (int)get_option('fb_import_time');
        $time_left = round((($last_imported + 3600) - time()) / 60);

        if($last_imported !== false && $time_left > 0 && $_POST['force_import'] !== 'true' ):            
            return;
        endif;


         /** IMPORT FACEBOOK */
        if(!empty(get_option('fb_access_token')) && !empty(get_option('fbpageid')) ):
            $facebook = get_fb_posts();
            if(!empty($facebook->data)):
                foreach($facebook->data as $fb):
                    $import_array[] = download_posts(
                        $fb->id,
                        $fb->message,
                        $fb->created_time,
                        $fb->full_picture,
                        $fb->permalink_url,
                        'facebook'
                    );
                endforeach;
            elseif(!empty($facebook->error)):
                error_log("Facebook Error:\n".date('jS F Y H:i:s').': '.$facebook->error->message."\nCode:".$facebook->error->code."\n",3, dirname(__DIR__).'/logs/error.log' );
                $error_code = $facebook->error->code;
                $error = '<tr><td><span class="error">Error: </span> - ';

                if($error_code == '10'):
                    $error .= "Facebook has not been granted permissions to access page data. To grant permission, update the 'Access Permissions' on the Connect page.";
                endif;


                $import_array[]['message'] = $error.'</td></tr>';
            endif;
        endif;
        if(empty(get_option('fb_access_token'))):
            $import_array[]['message'] = '<tr><td>Facebook Access Token required</td></tr>';
        else:
            if(empty(get_option('fbpageid'))):
                $import_array[]['message'] = '<tr><td>Facebook page not set.</td></tr>';
            endif;
        endif;
        

        /** IMPORT TWITTER */
        if(!empty(get_option('tw_post_limit'))):
            $limit = get_option('tw_post_limit');
        else:
            $limit = 5;
        endif;
        
        if(!empty(get_option('twitter_username'))):
            $tweets = get_tweets( $limit ,array('tweet_mode'=>'extended'));

            if(!empty($tweets)):
                foreach($tweets as $t):
                    $import_array[] = download_posts(
                        $t['id'],
                        $t['full_text'],
                        date('c', strtotime($t['created_at'])),
                        $t['entities']['media'][0]['media_url_https'],
                        "https://twitter.com/".get_option('twitter_username').'/status/'.$t['id'],
                        'twitter'
                    );
                endforeach;
            endif;
        else:
            $import_array[]['message'] = '<tr><td>Twitter Username not set</td></tr>';
        endif;

        /** IMPORT INSTAGRAM */
        if(!empty(get_option('ig_access_token'))):
            //Check if an Instagram profile has been manually selected
            $instagram = get_instagram();
            
        elseif(!empty(get_option('fb_access_token')) && !empty(get_option('fbpageid')) ):
            //If not try one associated with Facebook
            $instagram = instagram_via_facebook();
        endif;
        //...otherwise Instagram has nothing to import.

        

        if(!empty($instagram)):
            if(empty($instagram->error)):
                if(isset($instagram[0]->data)):
                    $instagram = $instagram[0]->data;
                endif;
                
                foreach($instagram as $ig):
                    $import_array[] = download_posts(
                        $ig->id,
                        $ig->caption,
                        $ig->timestamp,
                        $ig->media_url,
                        $ig->permalink,
                        'instagram'
                    );
                endforeach;
            else:
                error_log("Instagram Error:\n".date('jS F Y H:i:s').': '.$instagram->error->message."\nCode:".$instagram->error->code."\n",3, dirname(__DIR__).'/logs/error.log' );
                $error_code = $instagram->error->code;
                $error = '<tr><td><span class="error">Error: </span> - ';

                if($error_code == '10'):
                    $error .= "Instagram has not been granted permissions to access page data. To grant permission, update the 'Access Permissions' on the Connect page.";
                else:
                    $error .= $instagram->error;
                endif;


                $import_array[]['message'] = $error.'</td></tr>';

            endif;
        endif;


        if($display===true):
            echo '<table class="import_table">';
                foreach($import_array as $ia):
                    echo $ia['message'];
                endforeach;
            echo '</table>';
        endif;
        

        //IMPORT ALL SOCIAL POSTS TO WORDPRESS
        if(!empty($import_array)):
            foreach($import_array as $import):
                $i = $import['import'];

                if(empty($i)): continue; endif;
                
                $message = preg_replace('/[^\\w\d\s!@£#\$%&\(\):;\,\.\?\-\+]/', "",$i['social_message']);

                $import_post = [
                    'post_title'    => $i['post_title'],
                    'post_date'     => $i['post_date'],
                    'post_type'     => 'mso-social-posts',
                    'post_status'   => 'publish',
                    'meta_input'    => [
                        'social_id'       => $i['social_id'],
                        'social_message'  => $message,
                        'social_link'     => $i['social_link']
                    ]
                ];
                

                date_default_timezone_set('UTC');
                $insert = wp_insert_post($import_post);

                //If the import was successful, $insert will return the post_id as an integer value
                if(gettype($insert) == 'integer'):

                    //Assign the post to the corrisponding social channel category
                    wp_set_post_terms($insert,$i['social_channel'],'social-channels');
                    $import_message .= '<tr><td><strong>'.$i['post_title'].'</strong></td><td>successfully imported</td></tr>';

                    //ADD FUNCTION HERE TO DOWNLOAD AND SET IMAGE AS FEATURED IMAGE
                    if(!empty($i['social_image']) && !preg_match('/(video|mp4|m4v|webm)/',$i['social_image'])):
                        get_social_image($i['social_channel'][0].'_'.$i['social_id'],$i['social_image'],$insert,$i['social_link']);
                    endif;
                else:
                    $import_message .= '<tr><td><strong>'.$i['post_title'].'</strong></td><td><span style="color:red">unknown error</span></td></tr>';
                endif;
            endforeach;
        endif;

        if(!empty($import_message) && $display===true):
        echo '<table class="import_table">';
            echo $import_message;
        echo '</table>';
        endif;
        
    if($display===true):
    echo '</div>';
    endif;

    //Update the import time throttle
    update_option('fb_import_time',time());
}

