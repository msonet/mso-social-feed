<?php
//Block direct access
if ( ! defined( 'ABSPATH' ) ) exit;

/** CREATE PLUGIN NAV MENU ITEMS */
function mso_social_menu() {
    add_menu_page(
        'mso Social Feed',
        'mso Social Feed',
        'manage_options',
        'msosocial-feed',
        'mso_social_settings',
        'none',
        22
    );
    add_submenu_page(          //This is the parent but make it the first child too
        'msosocial-feed',          //Parent Slug
        'Connect',            //Page Title
        'Connect',            //Menu Title
        'manage_options',      //Capability
        'msosocial-feed',          //Menu Slug
        'mso_social_settings'      //Run Function (use this to in a way load the content for this page)
    );
    add_submenu_page(          
        'msosocial-feed',          //Parent Slug
        'Import',            //Page Title
        'Import',            //Menu Title
        'manage_options',     //Capability
        'msosocial-feed-import',  //Menu Slug
        'mso_social_import'       //Run Function (use this to in a way load the content for this page)
    );
    /*
    Uncomment to enable Social Categories for testing
    add_submenu_page(
        'msosocial-feed',
        'Social Channels',
        'Social Channels',
        'manage_options',
        'edit-tags.php?taxonomy=social-channels&post_type=mso-social-posts',  
    );
    */
}
add_action('admin_menu', 'mso_social_menu');
/** CREATE PLUGIN NAV MENU ITEMS */


/** FIX TAXONOMY NAV HIGHLIGHT */
add_action( 'parent_file', 'prefix_highlight_taxonomy_parent_menu' );
function prefix_highlight_taxonomy_parent_menu( $parent_file ) {
	if ( get_current_screen()->taxonomy == 'social-channels' ) {
		$parent_file = 'msosocial-feed';
	}
	return $parent_file;
}
/** FIX TAXONOMY NAV HIGHLIGHT */