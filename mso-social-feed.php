<?php
/*
Plugin Name: mso social feed
Plugin URI: https://bitbucket.org/msonet/mso-social-feed/
Description: Connect facebook, instagram and twitter accounts and regularly import your social media feeds into custom posts to be used on your website.
Author: mso web agency
Version: 1.0.0
Author URI: https://www.mso.net
Text Domain: mso-social-feed
Domain Path: /languages
License: GPLv2
*/

//Block direct access
if ( ! defined( 'ABSPATH' ) ) exit;

add_action( 'plugins_loaded', function() {

    include_once('api/fb.php');
    include_once('api/tw.php');
    include_once('api/ig.php');

    //Let's add some style to our plugin
    if(is_admin()):
    wp_enqueue_style('fontawesome', 'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', '', '20190528', 'all');
    wp_enqueue_style( 'mso_social_feed', plugin_dir_url( __FILE__ ) . 'css/mso_fb_feed.css', false, '1.0.4' );
    endif;


    //Let's include the necessary functions
    include_once('functions/plugin_pages.php');
    include_once('functions/posttype.php');
});

add_action('init', function(){
    include_once('functions/functions.php');

    //Plugin Pages
    include_once('pages/connect.php');
    include_once('pages/import.php');

    //Run the import function
    if(!isset($_POST['force_import'])):
    mso_import_posts(false);
    endif;
},9999);