=== mso social feed ===
Contributors: georgebroom
Tags: mso social feed, social feed, social feed to custom post, facebook, instagram, twitter,
Tested up to: 5.3.2
Stable tag: trunk

Simple plugin to connect facebook, instagram and twitter accounts and regularly import your social media feeds into custom posts to be used on your website.

== Description ==
The plugin creates a simple process of connecting facebook, instagram and twitter accounts to your wordpress website.

Posts added to the connected social media profiles are then imported into the website as a custom post. The custom posts can then be used on your website.