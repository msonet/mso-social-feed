<?php
//Block direct access
if ( ! defined( 'ABSPATH' ) ) exit;

function mso_social_settings(){ ?>


<div class="admin_container">
    <div class="tabs">
        <ul>
            <li><a href="?page=msosocial-feed"><i class="fa fa-facebook-f"></i> <i class="fa fa-instagram"></i></a></li>
            <li><a href="?page=msosocial-feed&social=twitter"><i class="fa fa-twitter"></i></a></li>
        </ul>

    </div>
    <div class="social_settings">

    <?php 
    //FACEBOOK'S CONNECT SETTINGS
    if(!isset($_GET['social'])): //Default load Facebook's settings 
        include_once('facebook.php');
    
    //TWITTER'S CONNECT SETTINGS
    elseif(isset($_GET['social']) && $_GET['social'] == 'twitter'):
        include_once('twitter.php');
  

    endif; ?>
    </div>
</div>

<?php } ?>