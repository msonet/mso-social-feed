<?php
//Twitter Settings Page

//Block direct access
if ( ! defined( 'ABSPATH' ) ) exit; ?>

<h1>Twitter</h1>

<form action="" method="post">
    <label>Twitter Username</label>
    <input type="text" name="twitter_username" value="<?= get_option('twitter_username'); ?>" />
    <br /><br />

    <?php /* Due to timeout errors limit is fixed to 5 posts
    
    <label>Twitter Post Import Limit<br />
    <em>Set how many posts to check and import</label>
    <input type="number" name="tw_post_limit" placeholder="5" value="<?= get_option('tw_post_limit'); ?>" />
    <br /><br />
    */ ?>

    <button class="fb_button" type="submit"> Save Options</button>
</form>

<hr />


<?php
$tweets = get_social_posts('twitter',10); 
if(!empty($tweets)):
    echo '<h2>Here are your latest imported tweets</h2>';
    echo '<p>Below is a preview of tweets that have been imported.</p>';
    echo '<ul class="post_preview">';

    foreach($tweets as $t):
        echo '<li>';
        echo '<a href="'.$t['image'].'" target="_blank"><img src="'.$t['image'].'" alt="" /></a>';
        echo '<div>';
        echo '<p>'.$t['date'].'</p>';
        echo '<p>'.$t['message'].'</p>';
        echo '<p><a href="'.$t['link'].'" target="_blank">Read more</a></p>';
        echo '</div>';
        echo '</li>';
    endforeach;
endif;
?>


</ul>