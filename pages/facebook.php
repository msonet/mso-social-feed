<?php
//Facebook Settings Page

//Block direct access
if ( ! defined( 'ABSPATH' ) ) exit;

$redirect = site_url().'/wp-admin/admin.php?page=msosocial-feed';
$page_id = get_option('fbpageid');
$token = get_option('fb_access_token');
$limit = get_option('fb_post_limit') ?? 5;
$app_secret = 'f3c43f1fcf9449a983b0e624cdadc359';
$secret_proof = hash_hmac('sha256', $token, $app_secret); 


$fb_logo = '<img width="16" height"16" style="padding-right: 5px;" src="https://en.facebookbrand.com/wp-content/themes/fb-branding/release/static/media/f-logo-white.1292990f.svg" />'; ?>

<h1>Facebook &amp; Instagram</h1>

<?php /** ACCESS TOKEN REQUEST FORM */ ?>
<form action="https://www.mso.net/facebook_oauth.php" method="post">
    <input type="hidden" name="redirect" value="<?= $redirect; ?>" />
    <input type="hidden" name="fb_oauth_request" value="yes" />
    <?php if(empty($token)): ?>
        <p>To load posts from Facebook and/or Instagram, you need to log in to Facebook and grant permission for this website to do so.</p>
        <p>This plugin does not post to Facebook or Instagram.</p>
        <button class="fb_button" type="submit"><?= $fb_logo; ?> Connect to Facebook</button>    
    <?php endif; ?>
</form>
<?php /** ACCESS TOKEN REQUEST FORM */ ?>



<?php /** DELETE ACCESS TOKEN FORM */ ?>
<?php if(!empty($token)): ?>
<h3>Access Token <input style="text-overflow: ellipsis;" type="text" readonly value="<?= $token; ?>" /></h3>

<form action="" method="post">


<p>An access token is used to to connect to Facebook and Instagram to check and obtain permissions and privileges connected to a particular account.
To remove access to your Facebook and Instagram accounts from this plugin, click the below button to delete the Access Token from this site.</p>
    <input type="hidden" name="social_platform" value="facebook" />
    <input type="hidden" name="remove_access_token" value="yes" />
    <button class="button" type="submit"><i class="fa fa-close"></i> Delete access token</button>
</form>
<hr />
<h3>Change Access Permissions</h3>
<form action="https://www.mso.net/facebook_oauth.php" method="post">
    <input type="hidden" name="redirect" value="<?= $redirect; ?>" />
    <input type="hidden" name="fb_oauth_request" value="yes" />
    <p>You can update the access level this site has by logging in to Facebook and selecting/deselecting which Facebook and Instagram pages/accounts you wish to grant this website access to. You can essentially use this feature to include/exclude either Facebook or Instagram.</p>

    <h4>Currently set permissions</h4>
    <p>To be able to import Facebook and Instagram posts please ensure all of the below have permissions.</p>
    <?php
        $url = "https://graph.facebook.com/me/permissions?access_token=${token}";
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_URL, $url );
        $result = json_decode(curl_exec( $ch ));
        curl_close( $ch ); ?>

        
        <ul class="permission_checklist">
        <?php foreach($result->data as $perm):
            if($perm->permission == 'email' || $perm->permission == 'public_profile'):
                continue;
            endif;

            if($perm->status == 'granted'):
                $permission = 'check';
            elseif($perm->status == 'declined'):
                $permission = 'times error';
            endif;

            $permission_name = $perm->permission;

            if($perm->permission == 'instagram_basic'):
                $permission_name = 'Access profile and posts from the Instagram account connected to your Page';
                if($perm->status == 'declined'):
                    $instagram_not_allowed = true;
                    update_option('ig_business_username','');
                endif;
            endif;

            if($perm->permission == 'manage_pages'):
                $permission_name = 'Manage your Pages';
            endif;

            if($perm->permission == 'pages_show_list'):
                $permission_name = 'Show a list of the Pages you manage';
            endif;
            ?>
            
            <li><?= $permission_name; ?>: <i class="fa fa-<?= $permission; ?>"></i></li>

        <?php endforeach; ?>
        </ul>

    <button class="fb_button" type="submit"><?= $fb_logo; ?> Update via Facebook</button>
</form>
<?php endif; ?>
<?php /** DELETE ACCESS TOKEN FORM */ ?>

<hr />

<?php /** FACEBOOK PAGE SELECT FORM */ ?>
<?php if(!empty($token)): ?>
    <form action="" method="post">
        <?php
        $url = "https://graph.facebook.com/me/accounts?limit=5&access_token=${token}";
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_URL, $url );
        $result = json_decode(curl_exec( $ch ));
        curl_close( $ch );
        if(!empty($result->data)):?>
        <h3>Select a Facebook Page</h3>
        <p>Instagram will pull from an Instagram Business account that is linked to the selected Facebook page, if applicable.</p>
        <select name="fbpageid">
            <option value="">- select a page -</option>
            <?php foreach($result->data as $d): ?>
                <option <?php if($page_id == $d->id): echo 'selected'; endif; ?> value="<?= $d->id; ?>"><?= $d->name; ?></option>
            <?php endforeach; ?>
        </select>
        <?php endif; ?>
    
        <br /><br />
        <?php /*
        Due to timeout errors limit is fixed to 5 posts
        <label>Facebook Post Import Limit<br />
        <em>Set how many posts to check and import</label>
        <input type="number" name="fb_post_limit" placeholder="5" value="<?= get_option('fb_post_limit'); ?>" />
        <br /><br />
        */ ?>
        <button class="button" type="submit"> Save Options</button>
        <?php if(isset($_POST['fbpageid'])): ?>
        <i class="fa fa-check form_saved"></i>

       <?php endif; ?>
    </form>
    <hr />
<?php endif; ?>
    
     
<h2><i class="fa fa-instagram"></i> Instagram</h2>
<?php
    $token = get_option('ig_access_token'); 
    $ig_username = get_option('ig_username');
    $ig_business_username = get_option('ig_business_username');
    $at_expires = get_option('ig_access_token_expires');

    if(!empty($token)): ?>
    <h3>Access Token <input style="text-overflow: ellipsis;" type="text" readonly value="<?= $token; ?>" /></h3>
    <?php
    if(!empty($at_expires) && $at_expires - time() > 1): ?>
        <p>Token Expires on: <?= date('jS F Y H:i', $at_expires ); ?></p>
    <?php else: ?>
        <p>Access Token has expired, please Update your Instagram settings to reconnect and obtain a new one.</p>
    <?php endif; endif;

    /** DELETE ACCESS TOKEN */
    if(!empty($token)): ?>
        <form action="" method="post">
            <input type="hidden" name="social_platform" value="instagram" />
            <input type="hidden" name="remove_access_token" value="yes" />
            <button class="button" type="submit"><i class="fa fa-close"></i> Delete access token</button>
        </form>
    <?php endif;

    //Display Connected Accounts
    if(!empty($ig_username)): ?>
        <h4>Currently Connected Instagram Account</h4>
        <ul>
        <li>- Personal: <strong><?= $ig_username; ?></strong></li>
        </ul>
    <?php endif; 
    
    if(!empty($ig_business_username)): ?>
        <?php if(!empty($ig_username)): ?>
            <h4>Also available</h4>
            <ul>
            <li>- Business: <strong><?= $ig_business_username; ?></strong> 
            <?php if($instagram_not_allowed === true): echo '<span class="error">Access Denied</span>'; endif; ?></li>
            </ul>
            <p><em>In order to use the <strong><?= $ig_business_username; ?></strong> account you must disconnect
            your personal account by removing the Instagram Access Token.</p>
        <?php else: ?>
            <h4>Currently Connected Instagram Account</h4>
            <ul>
            <li>- Business: <strong><?= $ig_business_username; ?></strong>
            <?php if($instagram_not_allowed === true): echo '<span class="error">Access Denied</span>'; endif; ?></li>
            </ul>
        <?php endif; ?>
        
    <?php endif; ?>

    <?php if(empty($ig_username) && empty($ig_business_username)): ?>
        <p>No Instagram accounts connected. You can either connect your Facebook page to an Instagram account
        or you can directly connect to an Instagram account using the buttons below.</p>
    <?php endif;

    

/** ACCESS TOKEN REQUEST FORM */ ?>
<form action="https://www.mso.net/instagram_oauth.php" method="post">
    <input type="hidden" name="redirect" value="<?= $redirect; ?>" />
    <input type="hidden" name="ig_oauth_request" value="yes" />

    <?php if(empty($token)): ?>
        <button class="fb_button instagram" type="submit"><i class="fa fa-instagram"></i> Connect to Instagram</button>
        <?php if(!empty($ig_business_username)): ?>
        <br />
        <em>The account you connect to will be used instead of <?= $ig_business_username; ?>.</em>
        <?php endif; ?>
    <?php else: ?>
        <button class="fb_button instagram" type="submit"><i class="fa fa-instagram"></i> Update via Instagram</button>    
    <?php endif; ?>
</form>
<?php /** ACCESS TOKEN REQUEST FORM */ ?>
    


<?php /** FACEBOOK PAGE SELECT FORM */ ?>