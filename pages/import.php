<?php
//Block direct access
if ( ! defined( 'ABSPATH' ) ) exit;



function mso_social_import(){
    //Run the import function if we submit the button
    mso_import_posts(true);
    
    //Otherwise show the button
    $last_imported = (int)get_option('fb_import_time');
    $time_left = round((($last_imported + 3600) - time()) / 60);

    if(!isset($_POST['force_import'])):

    echo '<div class="social_settings import">';
    echo '<div class="import_delay">';
    echo 'Next automatic import will be available after '.$time_left.' minutes';
    echo '<br /><form action="" method="post"><button name="force_import" value="true" type="submit"><span>Import Now</span> <i class="fa fa-cloud-download" aria-hidden="true"></i></button>';
    echo '</div>';
    echo '</div>';
    
    endif;
}