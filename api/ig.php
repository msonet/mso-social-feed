<?php
//Block direct access
if ( ! defined( 'ABSPATH' ) ) exit;

//Access token obtained from https://www.mso.net/instagram_oauth.php as per the Api app's "Valid OAuth Redirect URIs list"
if(isset($_GET['ig_access_token'])):
    
    //Update the option value in the database
    update_option('ig_access_token',$_GET['ig_access_token']);
    
    if(isset($_GET['ig_id'])):
        update_option('ig_id',$_GET['ig_id']);
        $ig_id = $_GET['ig_id'];
        $token = $_GET['ig_access_token'];

        $url = "https://graph.instagram.com/${ig_id}?fields=id,username&access_token=${token}";
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_URL, $url );
        $result = json_decode(curl_exec( $ch ));
        curl_close( $ch );
        
        update_option('ig_username',$result->username);
    endif;

    //Exchange the token for a long lived (3 month) token
    $app_secret = '9e52d0f0701658c42bf76f876b7797b2';
    $url = "https://graph.instagram.com/access_token?grant_type=ig_exchange_token&client_secret=${app_secret}&access_token=${token}";
     
    $ch = curl_init();
    curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch, CURLOPT_URL, $url );
    $result = json_decode(curl_exec( $ch ));
    curl_close( $ch );
    
    
    $token = $result->access_token;
    update_option('ig_access_token',$token);
    update_option('ig_access_token_expires',time() + $result->expires_in);

    header("location:".site_url().'/wp-admin/admin.php?page=msosocial-feed');
endif;

/** GET INSTAGRAM PAGE ASSOCIATED WITH FACEBOOK ACCOUNT */
function instagram_via_facebook(){
    $page_id = get_option('fbpageid');
    $token = get_option('fb_access_token');
    $app_secret = 'f3c43f1fcf9449a983b0e624cdadc359';
    $secret_proof = hash_hmac('sha256', $token, $app_secret); 

    if(!empty($token) && !empty($page_id)):
        //Get the user ID
        $url = "https://graph.facebook.com/v6.0/".get_option('fbpageid')."?fields=instagram_business_account&access_token=${token}&appsecret_proof=${secret_proof}";
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_URL, $url );
        $result = json_decode(curl_exec( $ch ));
        curl_close( $ch );

        if($result->error)
            return $result;

        if(!empty(get_option('ig_post_limit'))):
            $limit = get_option('ig_post_limit');
        else:
            $limit = 5;
        endif;


        //Use user ID to obtain page's data
        if(!empty($result->instagram_business_account->id)):
            $ig_id = $result->instagram_business_account->id;
            $url = "https://graph.facebook.com/v6.0/${ig_id}/media?limit=${limit}&access_token=${token}";
        elseif(!empty($result->id)):
            if($result->id == $page_id):
                $result->error = 'No Instagram Business Account associated with this Facebook page.';
                return $result;
            endif;
        endif;
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_URL, $url );
        $result = json_decode(curl_exec( $ch ));
        curl_close( $ch );

        
        
        //Use the page ID to obtain the data
        if(!empty($result->data)):
            foreach($result->data as $i):
                $id = $i->id;
                $url = "https://graph.facebook.com/v6.0/${id}?fields=id,timestamp,media_url,caption,permalink&access_token=${token}";
                $ch = curl_init();
                curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
                curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
                curl_setopt( $ch, CURLOPT_URL, $url );
                $output[] = json_decode(curl_exec( $ch ));
                curl_close( $ch );
            endforeach;
        endif;


        return $output;

    endif;
}
/** GET INSTAGRAM PAGE ASSOCIATED WITH FACEBOOK ACCOUNT */




//Get instagram using instagram's API if not connecting via Facebook
function get_instagram(){
    
    $token = get_option('ig_access_token');
    $expires = get_option('ig_access_token_expires');
    $app_secret = '9e52d0f0701658c42bf76f876b7797b2';
    $secret_proof = hash_hmac('sha256', $token, $app_secret);

    if(!empty($token)):
        //Exchange the token for a long lived (3 month) token
        if( (empty($expires) && !empty($token)) || ( $at_expires - time() < 1 ) ):
            $url = "https://graph.instagram.com/access_token?grant_type=ig_exchange_token&client_secret=${app_secret}&access_token=${token}";
        else:
        //Refresh the long lived token
            $url = "https://graph.instagram.com/refresh_access_token?grant_type=ig_refresh_token&access_token=${token}";
        endif;
        
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_URL, $url );
        $result = json_decode(curl_exec( $ch ));
        curl_close( $ch );
        
        
        $token = $result->access_token;
        update_option('ig_access_token',$token);
        update_option('ig_access_token_expires',time() + $result->expires_in);
        
        $url = "https://graph.instagram.com/me/media?limit=5&fields=id,timestamp,media_url,caption,permalink&access_token=${token}";
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_URL, $url );
        $output[] = json_decode(curl_exec( $ch ));
        curl_close( $ch );

        return $output;
    endif;
}
//Get instagram using instagram's API if not connecting via Facebook



/** FORM SUBMISSION HANDLING */

//Remove the access token from the database
if(isset($_POST['remove_access_token']) && $_POST['remove_access_token'] == 'yes' && $_POST['social_platform'] == 'instagram'):
    update_option('ig_access_token','');
    update_option('ig_username','');
    update_option('ig_access_token_expires','');
    //Redirect to the plugin page thus dropping the "code" parameter
    header("location:".site_url().'/wp-admin/admin.php?page=msosocial-feed');
endif;

//Set the Instagram Post Import Limit
if(isset($_POST['ig_post_limit'])):
    update_option('ig_post_limit',$_POST['ig_post_limit']);
    //Redirect to the plugin page thus dropping the "code" parameter
    header("location:".site_url().'/wp-admin/admin.php?page=msosocial-feed');
endif;

/** FORM SUBMISSION HANDLING */