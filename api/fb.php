<?php
//Block direct access
if ( ! defined( 'ABSPATH' ) ) exit;

/* SET THE FACEBOOK ACCESS TOKEN
Process:
- 1 Submit form with request to https://www.mso.net/facebook_oauth.php
- 2a File stores the redirect to a cookie (5 minutes expiry time) and redirects user to Facebook
- 2b The file is the only URI in the valid OAuth redirects so is required for this process.
- 3 Once authenticated Facebook redirects the user back to https://www.mso.net/facebook_oauth.php with a code
- 4 facebook_oauth.php pings Facebook with the acquired code to receive the access token
- 5 facebook_oauth.php redirects the user back to the stored redirect URL (/wp-admin/admin.php?page=msosocial-feed) with the access token
- 6 The access token is then saved in the database below....
*/

//Access token obtained from https://www.mso.net/facebook_oauth.php as per the Api app's "Valid OAuth Redirect URIs list"
if(isset($_GET['fb_access_token'])):
    //Update the option value in the database
    update_option('fb_access_token',$_GET['fb_access_token']);
    header("location:".site_url().'/wp-admin/admin.php?page=msosocial-feed');
endif;
/* SET THE FACEBOOK ACCESS TOKEN */


/** OBTAIN FACEBOOK POSTS*/
function get_fb_posts(){
    //Get the posts and store them in the database - check against the post's Facebook ID
    if(!empty( get_option('fb_access_token') )):
        $page_id = get_option( 'fbpageid');
        $token = get_option( 'fb_access_token');
        $limit = strlen(get_option( 'fb_post_limit') > 0) ? get_option( 'fb_post_limit') : 5;
        $app_secret = 'f3c43f1fcf9449a983b0e624cdadc359';
        $secret_proof = hash_hmac('sha256', $token, $app_secret); 

        $url = "https://graph.facebook.com/${page_id}/feed?fields=full_picture,picture,message,created_time,permalink_url&limit=${limit}&access_token=${token}&appsecret_proof=${secret_proof}";

        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_URL, $url );
        $result = json_decode(curl_exec( $ch ));
        curl_close( $ch );
        
        return $result;
    endif;
}
/** OBTAIN FACEBOOK POSTS*/


/** FORM SUBMISSION HANDLING */

//Remove the access token from the database
if(isset($_POST['remove_access_token']) && $_POST['remove_access_token'] == 'yes' && $_POST['social_platform'] == 'facebook'):
    update_option('fb_access_token','');
    update_option('fbpageid','');
    update_option('ig_business_username','');
    //Redirect to the plugin page thus dropping the "code" parameter
    header("location:".site_url().'/wp-admin/admin.php?page=msosocial-feed');
endif;

//Set the Facebook Page ID value
if(isset($_POST['fbpageid'])):
    update_option('fbpageid',$_POST['fbpageid']);

    $token = get_option( 'fb_access_token');
    $app_secret = 'f3c43f1fcf9449a983b0e624cdadc359';
    $secret_proof = hash_hmac('sha256', $token, $app_secret); 

    //Update Instagram Business ID and username
    
    //Get the Instagram Business ID
    $url = "https://graph.facebook.com/v6.0/".$_POST['fbpageid']."?fields=instagram_business_account&access_token=${token}&appsecret_proof=${secret_proof}";
    $ch = curl_init();
    curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch, CURLOPT_URL, $url );
    $result = json_decode(curl_exec( $ch ));
    curl_close( $ch );

    $ig_b_id = $result->instagram_business_account->id;
    
    //Set the Instagram Business ID and Username
    if(!empty($ig_b_id)):
        $url = "https://graph.facebook.com/v6.0/${ig_b_id}?fields=id,username&access_token=${token}";
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_URL, $url );
        $result = json_decode(curl_exec( $ch ));
        curl_close( $ch );
        
        
        update_option('ig_business_id',$result->id);
        update_option('ig_business_username',$result->username);
    else:
        update_option('ig_business_id','');
        update_option('ig_business_username','');
    endif;
 
endif;

//Set the Facebook Post Import Limit
if(isset($_POST['fb_post_limit'])):
    update_option('fb_post_limit',$_POST['fb_post_limit']);

endif;

/** FORM SUBMISSION HANDLING */