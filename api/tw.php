<?php
//Block direct access
if ( ! defined( 'ABSPATH' ) ) exit;

/*
 * Get tweets
 */

require('inc/twitter/twitter.class.php');

/* implement get_tweets */
function get_tweets($count = 5, $options = false) {
  $consumer_key = 'kmyOVnYrTqsnRbGBr5EU0W9jS';
  $consumer_secret = 'dhS025jajrLFTnXV0TRP2G0cULolAzbtxlyXgqDJf9FDRGHPMa';
  $access_token = '198526166-2zoXOzoYltlb2Hiyg7r4tDtslApJ3CQaRY1wLBrF';
  $token_secret = 'OX13zEP2m4CXidycx6EijHK2ncAXXsU4sSRlVqZmT22n2';

  $twitter = [
      'username' => get_option('twitter_username'),
      'consumer_key' => $consumer_key,
      'consumer_secret' => $consumer_secret,
      'access_token' => $access_token,
      'access_token_secret' => $token_secret,
  ];
  
  $config['key'] = $twitter['consumer_key'];
  $config['secret'] = $twitter['consumer_secret'];
  $config['token'] = $twitter['access_token'];
  $config['token_secret'] = $twitter['access_token_secret'];
  $config['screenname'] = $twitter['username'];
  $config['cache_expire'] = 0;



  if ($config['cache_expire'] < 1) {
    $config['cache_expire'] = 0;
  }
  $config['directory'] = $upload_dir['basedir'] . '/';

  $obj = new Twitter($config);
  $res = $obj->getTweets($twitter['username'], $count, $options);
  update_option('tdf_last_error', $obj->st_last_error);
  return $res;
}

function parse_tweet_message($text) {
  //links
  $text = preg_replace('@(https?://([-\w\.]+)+(/([\w/_\.]*(\?\S+)?(#\S+)?)?)?)@', '<a href="$1" target="_blank">$1</a>', $text);

  //users
  $text = preg_replace('/@(\w+)/', '<a href="http://twitter.com/$1" target="_blank">@$1</a>', $text);

  //hashtags
  $text = preg_replace('/\s+#(\w+)/', ' <a href="https://twitter.com/hashtag/$1" target="_blank">#$1</a>', $text);

  return $text;
}


/** FORM SUBMISSION HANDLING */

//Set the Twitter username to import from
if(isset($_POST['twitter_username'])):
    update_option('twitter_username',$_POST['twitter_username']);
    //Redirect to the plugin page thus dropping the "code" parameter
    header("location:".site_url().'/wp-admin/admin.php?page=msosocial-feed&social=twitter');
endif;

//Set the Twitter Post Import Limit
if(isset($_POST['tw_post_limit'])):
    update_option('tw_post_limit',$_POST['tw_post_limit']);
    //Redirect to the plugin page thus dropping the "code" parameter
    header("location:".site_url().'/wp-admin/admin.php?page=msosocial-feed&social=twitter');
endif;

/** FORM SUBMISSION HANDLING */

